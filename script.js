let counterMilisec = 0;
let counterSec = 1;
let counterMin = 1;
let startIntervalmilisec;
let startIntervalsec;
let startIntervalmin;
const getId = id => document.getElementById(id);

// Милисекунды
const count = () => {
   getId("milisec").innerText = "0" + counterMilisec;
   counterMilisec++;
   if (counterMilisec >= 10) {
      getId("milisec").innerText = counterMilisec;
   };
   if (counterMilisec == 99) {
      counterMilisec = 0
   };
   if (counterMilisec == 99) {
      counterSec = counterSec + 1;
   }
}

// Секунды
const count2 = () => {
   getId("sec").innerText = "0" + counterSec;
   counterSec++;
   if (counterSec >= 10) {
      getId("sec").innerText = counterSec;
   };
   if (counterSec == 59) {
      counterSec = 0
   };
}

// Минуты
const count3 = () => {
   getId("min").innerText = "0" + counterMin;
   counterMin++;
   if (counterMin >= 10) {
      getId("min").innerText = counterMin;
   };
   if (counterMin == 59) {
      counterMin = 0
   };
}

//Кнопка Старт (запуск)

getId("Start").onclick = () => {
   startIntervalmilisec = setInterval(count, 10);
   startIntervalsec = setInterval(count2, 1000);
   startIntervalmin = setInterval(count3, 60000);
   getId("Start").setAttribute('disabled', true);
   getId("Start").classList.remove("button");
   getId("Start").classList.add("start-button");

   getId("IconStart").classList.add("fa-fade");
   getId("IconStart").style.color = "rgba(3, 172, 11, 0.5)";

   getId("Stop").classList.remove("stop-button");
   getId("Stop").classList.add("button");

   getId("IconStop").classList.remove("fa-fade");
   getId("IconStop").style.color = "black";

   getId("Reset").classList.remove("reset-button");
   getId("Reset").classList.add("button");
   getId("IconReset").style.color = "black";
   
   getId("maineC").classList.remove("container-stopwatch");
   getId("maineC").classList.remove("stop-display");
   getId("maineC").classList.remove("reset-display");
   getId("maineC").classList.add("start-display");

   getId("display").classList.remove("stopwatch-display");
   getId("display").classList.add("green");

}

// Кнопка СТОП
getId("Stop").onclick = () => {
   clearInterval(startIntervalmilisec);
   clearInterval(startIntervalsec);
   clearInterval(startIntervalmin);
   getId("Start").removeAttribute('disabled');
   getId("Start").classList.remove("start-button");
   getId("Start").classList.add("button");

   getId("IconStart").classList.remove("fa-fade");
   getId("IconStart").style.color = "black";

   getId("Reset").classList.remove("reset-button");
   getId("Reset").classList.add("button");
   getId("IconReset").style.color = "black";
   
   getId("Stop").classList.add("stop-button");
   getId("IconStop").classList.add("fa-fade");
   getId("IconStop").style.color = "rgba(172, 14, 3, 0.5)";
   
   getId("maineC").classList.remove("start-display");
   getId("maineC").classList.remove("reset-display");
   getId("maineC").classList.add("stop-display");

   getId("display").classList.remove("green");
   getId("display").classList.add("red");
}

// Кнопка Сброс
getId("Reset").onclick = () => {
   clearInterval(startIntervalmilisec);
   clearInterval(startIntervalsec);
   clearInterval(startIntervalmin);
   getId("Start").removeAttribute('disabled');
   getId("Start").classList.remove("start-button");
   getId("Start").classList.add("button");

   getId("IconStart").classList.remove("fa-fade");
   getId("IconStart").style.color = "black";

   counterMilisec = 0;
   counterSec = 1;
   counterMin = 1;
   let c1 = "00";
   let c2 = "00";
   let c3 = "00";
   getId("milisec").innerText = c1;
   getId("sec").innerText = c2;
   getId("min").innerText = c3;

   getId("maineC").classList.remove("stop-display");
   getId("maineC").classList.remove("start-display");
   getId("maineC").classList.add("reset-display");

   getId("Reset").classList.remove("button");
   getId("Reset").classList.add("reset-button");
   getId("IconReset").style.color = "rgba(3, 87, 172, 0.5)";

   getId("Stop").classList.remove("stop-button");
   getId("Stop").classList.add("button");

   getId("IconStop").classList.remove("fa-fade");
   getId("IconStop").style.color = "black";

   getId("display").classList.remove("red");
   getId("display").classList.add("silver");
}